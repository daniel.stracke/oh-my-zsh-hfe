# oh-my-zsh install
This script eases the installation of some neat zsh features.

- file manager `lf`
- autojump function with `zoxide`
- fuzzy finder `fzf`
- autosuggestions
- and some minor things like shell git integration

## Sane shortcuts
- `Ctrl + t`: Fuzzy search
- `Ctrl + r`: Fuzzy search shell history
- `n`: file manager
- `Space + tab` (while typing some directory name): autocompletion of previously used directories
