#!/bin/zsh

SHELL=/bin/zsh
if [ ! -f "$SHELL" ]; then
	echo "zsh not installed! Install first before running this script!"
	exit 1;
fi

read

git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

ZSHRC="~/.zshrc"
if [ -f "$ZSHRC" ]; then
	cp ~/.zshrc ~/.zshrc.orig
	echo "backup .zshrc to .zshrc.orig"
fi
cp ./oh-my-zsh-data/templaterc ~/.zshrc

# Nerd Fonts
mkdir -p $HOME/.fonts
curl https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf -o ~/.fonts/MesloLGS\ NF\ Regular.ttf
curl https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf -o ~/.fonts/MesloLGS\ NF\ Italic.ttf
curl https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf -o ~/.fonts/MesloLGS\ NF\ Bold.ttf
curl https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf -o ~/.fonts/MesloLGS\ NF\ Bold\ Italic.ttf

# update the font cache
fc-cache -f -v

# Autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# lf explorer
mkdir -p $HOME/.local/bin/
cd $HOME/.local/bin
wget https://github.com/gokcehan/lf/releases/download/r31/lf-linux-amd64.tar.gz
tar xvfz lf-linux-amd64.tar.gz
rm lf-linux-amd64.tar.gz

mkdir -p $HOME/.config/lf
cp /mnt/work/tmp2/daario/dsommerweisel/.oh-my-zsh-data/lf/* $HOME/.config/lf/

# zoxide
curl -sS https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | zsh

read

# Rust
export HOME_NVME=/mnt/work/tmplocal/
mkdir $HOME_NVME/.cargo
mkdir $HOME_NVME/.rustup
ln -s $HOME_NVME/.cargo $HOME/.cargo
ln -s $HOME_NVME/.rustup $HOME/.rustup
export PATH="$HOME/.local/bin/:$PATH"

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh


# rust based tools
cargo install fd-find ripgrep bat cargo-cache
cargo cache -a
